from typing import TypeVar, Generic, Dict, List, Tuple

T = TypeVar("T")


class MinHeap(Generic[T]):

    def __init__(self, capacity=8):
        self.__tree: List[Tuple[int, T]] = [(0, None)] * capacity
        self.__position_dict: Dict[T, int] = {}

    def __parent(self, i: int) -> int:
        return i // 2

    def __left(self, i: int) -> int:
        return i * 2

    def __right(self, i: int) -> int:
        return i * 2 + 1

    def __key(self, i: int) -> int:
        return self.__tree[i][0]

    def __value(self, i: int) -> T:
        return self.__tree[i][1]

    def __bubble_up(self, i: int):
        if i == 0:
            return
        p = self.__parent(i)
        if self.__key(p) > self.__key(i):
            self.__tree[p], self.__tree[i] = self.__tree[i], self.__tree[p]
            self.__position_dict[self.__value(p)] = p
            self.__position_dict[self.__value(i)] = i
            self.__bubble_up(p)

    def __trickle_down(self, i: int):
        c = self.__left(i)
        if c >= len(self.__position_dict):
            return
        r = self.__right(i)

        if r < len(self.__position_dict) and self.__key(c) > self.__key(r):
            c = r

        if self.__key(c) < self.__key(i):
            self.__tree[c], self.__tree[i] = self.__tree[i], self.__tree[c]
            self.__position_dict[self.__value(c)] = c
            self.__position_dict[self.__value(i)] = i
            self.__trickle_down(c)

    def add(self, key: int, value: T):
        if len(self.__position_dict) == len(self.__tree):
            self.__tree.extend([(0, None)] * 16)
        pos = len(self.__position_dict)
        self.__tree[pos] = (key, value)
        self.__position_dict[value] = pos
        self.__bubble_up(pos)

    def extract_min(self) -> T:
        value = self.__value(0)
        self.__position_dict.pop(value)
        size = len(self.__position_dict)
        if size > 0:
            self.__tree[0] = self.__tree[size]
            self.__position_dict[self.__value(0)] = 0
            self.__trickle_down(0)
        self.__tree[size] = None
        return value

    def key(self, value: T) -> int:
        return self.__key(self.__position_dict[value])

    def decrease_key(self, value: T, new_key: int):
        old_key = self.key(value)
        if old_key > new_key:
            pos = self.__position_dict[value]
            self.__tree[pos] = (new_key, value)
            self.__bubble_up(pos)

    def is_empty(self) -> bool:
        return len(self.__position_dict) == 0

    def contains(self, item: T) -> bool:
        return item in self.__position_dict
