# Euristica assegnata = Closest Insertino
from datetime import datetime
from typing import List, Tuple, Callable, Dict
import re
import math
import glob

from algorithms import hk_visit, HkTimeout, closest_insertion, prim_mst
from graph import Graph, BitMask


def to_rad(x: float) -> float:
    PI = 3.141592
    deg = int(x)
    min = x - deg
    return PI * (deg + 5.0 * min / 3.0) / 180.0


def geo_distance(lat1: float, long1: float, lat2: float, long2: float) -> int:
    lat1_rad = to_rad(lat1)
    long1_rad = to_rad(long1)
    lat2_rad = to_rad(lat2)
    long2_rad = to_rad(long2)

    RRR = 6378.388
    q1 = math.cos(long1_rad - long2_rad)
    q2 = math.cos(lat1_rad - lat2_rad)
    q3 = math.cos(lat1_rad + lat2_rad)

    return int(RRR * math.acos(0.5 * ((1.0 + q1) * q2 - (1.0 - q1) * q3)) + 1.0)


def euclidean_distance(x1: float, y1: float, x2: float, y2: float) -> int:
    return int(math.sqrt(math.pow((x1-x2), 2) + math.pow(y1-y2, 2)))


def parse_file(path: str) -> Graph:
    name = ""
    coords: List[Tuple[float, float]] = []
    edge_type = ""
    with open(path, 'r') as file:
        name = file.readline().replace('\n', '').split(':')[1].strip()
        file.readline()  # Skip type
        file.readline()  # skip commend
        node_count = int(file.readline().split(':')[1].strip())
        edge_type = file.readline().split(':')[1].strip()
        line = ""
        while "NODE_COORD_SECTION" not in line:
            line = file.readline()

        tokenizer = re.compile('\s+')
        for i in range(node_count):
            line = file.readline().replace('\n', '').lstrip()
            items = tokenizer.split(line)
            coords.append((float(items[1]), float(items[2])))

    edge_fun : Callable[[float, float, float, float], int] = None
    if edge_type == "GEO":
        edge_fun = geo_distance
    elif edge_type == "EUC_2D":
        edge_fun = euclidean_distance
    else:
        raise Exception("Unsupported edge type {0}".format(edge_type))

    graph: Graph = Graph(name, len(coords))
    for i in range(len(coords)):
        for j in range(len(coords)):
            if i == j:
                continue
            tail_coord = coords[i]
            head_coord = coords[j]
            graph.connect(i, j, edge_fun(tail_coord[0], tail_coord[1], head_coord[0], head_coord[1]))
    return graph


def parse_graphs() -> List[Graph]:
    files = glob.glob("./data/*.tsp")
    graphs = []
    for file in files:
        graphs.append(parse_file(file))

    return graphs


def main():
    graphs = parse_graphs()
    for graph in graphs:
        print("Computing on {0}".format(graph.name))

        S = BitMask()
        for i in range(1, graph.size):
            S.add(i)

        timeout = HkTimeout(120)
        start = datetime.now()
        result = hk_visit(graph, {}, 0, S, timeout)
        if timeout.expired():
            print("HK parziale {0} ex time {1}".format(result, 120))
        else:
            delta = (datetime.now() - start)
            print("HK {0} ex time {1}".format(result, delta))


        csi = closest_insertion(graph)
        print("Closest insertion = {0}".format(csi))
        prim, tree = prim_mst(graph, 0)
        print("2 Approssimato = {0}".format(prim))
        print("")


if __name__ == '__main__':
    main()
