from typing import List


class Graph:

    def __init__(self, name: str, size: int):
        self.__name = name
        self.__size = size
        self.__weights = [[-1 for x in range(size)] for y in range(size)]

    def weight(self, tail: int, head: int) -> int:
        weight_val = self.__weights[tail][head]
        if weight_val < 0:
            raise Exception("{0} {1} are disconnected".format(tail, head))
        return weight_val

    def connect(self, tail: int, head: int, weight: int = 0):
        self.__weights[tail][head] = weight

    def disconnect(self, tail, head):
        if self.__weights[tail][head] < 0:
            raise Exception("{0} {1} already disconnected".format(tail, head))
        self.__weights[tail][head] = -1
        self.__weights[head][tail] = -1

    def are_connected(self, tail, head):
        return self.__weights[tail][head] >= 0

    def neighbours(self, v: int) -> List[int]:
        neighbours = []
        row = self.__weights[v]
        for i in range(len(row)):
            if row[i] >= 0:
                neighbours.append(i)
        return neighbours

    def __get_name(self) -> str:
        return self.__name

    def __get_size(self):
        return self.__size

    def __iter__(self):
        return iter(range(self.__size))

    size = property(__get_size)

    name = property(__get_name)


class BitMask:

    def __init__(self):
        self.__bits = 0
        self.__size = 0

    def __set_bit(self, i: int):
        self.__bits |= 1 << i

    def __un_set_bit(self, i: int):
        self.__bits &= ~(1 << i)

    def __get_bit(self, i: int) -> int:
        return (self.__bits & (1 << i)) >> i

    def add(self, i: int):
        self.__set_bit(i)
        self.__size += 1

    def contains(self, i: int) -> bool:
        return self.__get_bit(i) > 0

    def remove(self, i: int):
        self.__un_set_bit(i)
        self.__size -= 1

    def clone(self):
        copy = BitMask()
        copy.__bits = self.__bits
        copy.__size = self.__size
        return copy

    def size(self) -> int:
        return self.__size

    def elements(self) -> List[int]:
        result = []
        for x in range(self.__bits.bit_length()):
            if self.contains(x):
                result.append(x)
        return result

    def __iter__(self):
        return iter(self.elements())

    def __eq__(self, other) -> bool:
        if type(other) is int:
            return self.__bits == (1 << int(other))
        elif type(other) is BitMask:
            return self.__bits == other.__bits
        else:
            return False

    def __hash__(self):
        return self.__bits

    def __contains__(self, item):
        if item is int:
            return self.contains(item)
        else:
            return False
