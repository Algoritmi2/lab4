from typing import Tuple, Dict, List, Set

from graph import Graph
from heap import MinHeap
import math
from performance import execution_time


def BFS_ric(node: int, tree: Dict[int, List[int]], visited: Set[int]) -> List[int]:
    result = [node]
    visited.add(node)
    if node in tree:
        for child in tree[node]:
            if child not in visited:
                result.extend(BFS_ric(child, tree, visited))
    return result


def BFS_visit(node: int, tree: Dict[int, List[int]], graph: Graph) -> int:
    path = BFS_ric(0, tree, set())
    path.append(node)
    result = 0
    for i in range(0, len(path) - 1):
        result += graph.weight(path[i], path[i+1])
    return result


def to_tree(start: int, pi: Dict[int, int]) -> Dict[int, List[int]]:
    tree: Dict[int, List[int]] = {}
    for key, value in pi.items():
        if key == start:
            continue
        sub_nodes = tree.get(value, [])
        sub_nodes.append(key)
        tree[value] = sub_nodes
    return tree


@execution_time
def prim_mst(graph: Graph, r: int) -> Tuple[int, Dict[int, List[int]]]:
    min_heap: MinHeap[int] = MinHeap(graph.size)
    pi: Dict[int, int] = {}
    for v in graph:
        min_heap.add(math.inf, v)
        pi[v] = None

    min_heap.decrease_key(r, 0)
    while not min_heap.is_empty():
        u = min_heap.extract_min()
        for v in graph.neighbours(u):
            if min_heap.contains(v):
                old_key = min_heap.key(v)
                new_key = graph.weight(u, v)
                if new_key < old_key:
                    min_heap.decrease_key(v, new_key)
                    pi[v] = u

    tree = to_tree(r, pi)
    return BFS_visit(0, tree, graph), tree
