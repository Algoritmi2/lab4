import math
from typing import Dict
from graph import Graph
from performance import execution_time


def delta(graph: Graph, k: int, edges: Dict[int, Dict[int, int]]) -> int:
    min_val = math.inf
    for h in edges:
        if graph.are_connected(h, k):
            weight = graph.weight(h, k)
            if weight < min_val:
                min_val = weight
    return min_val


@execution_time
def closest_insertion(graph: Graph) -> int:
    edges: Dict[int, Dict[int, int]] = {}

    # Initiazlization
    min_val = math.inf
    min_j = -1
    for j in graph.neighbours(0):
        weight = graph.weight(0, j)
        if weight < min_val:
            min_j = j
            min_val = weight
    edges[min_j] = {0: min_val}
    edges[0] = {min_j: min_val}

    while len(edges) != graph.size:
        # Select the closest node
        k = -1
        min_delta = math.inf
        for v in graph:
            if v in edges:
                continue
            d = delta(graph, v, edges)
            if d < min_delta:
                min_delta = d
                k = v
        if k == -1:
            raise Exception("Can't add other nodes to the path.")

        # Insertion
        min_i, min_j = -1, -1
        min_val = math.inf
        for i in edges.keys():
            for j in edges[i].keys():
                val = graph.weight(i, k) + graph.weight(k, j) - graph.weight(i, j)
                if val < min_val:
                    min_val = val
                    min_i, min_j = i, j

        edges[min_i].pop(min_j)
        edges[min_i][k] = graph.weight(min_i, k)
        edges[k] = {min_j: graph.weight(k, min_j)}
    return sum(map(lambda x: list(edges[x].values())[0], edges.keys()))
