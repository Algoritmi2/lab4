from datetime import datetime, timedelta
from typing import Dict
import math

from graph import Graph, BitMask


class HkTimeout:

    def __init__(self, sec=-1):
        if sec >= 0:
            self.__end = datetime.now() + timedelta(seconds=sec)
        else:
            self.__end = None

    def expired(self) -> bool:
        return self.__end is not None and datetime.now() >= self.__end


def hk_visit(graph: Graph, dist: Dict[int, Dict[BitMask, int]], v: int, S: BitMask, timeout: HkTimeout = HkTimeout()) -> int:
    if timeout.expired():
        return math.inf
    if S == v:
        return graph.weight(v, 0)
    elif v in dist and S in dist[v]:
        return dist[v][S]
    else:
        mindist = math.inf
        # minprec = None
        for u in S:
            if u == v:
                continue
            S_copy = S.clone()
            S_copy.remove(v)
            result_dist = hk_visit(graph, dist, u, S_copy, timeout)
            if result_dist + graph.weight(u, v) < mindist:
                mindist = result_dist + graph.weight(u, v)
                # minprec = u
        if v not in dist:
            dist[v] = {}
        dist[v][S] = mindist
    return mindist